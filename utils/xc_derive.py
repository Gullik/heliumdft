#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  xc_derive.py
#
#  Copyright 2018 Martin Gulliksson <martin@martingulliksson.com>
#             and John Daniel Bossér <john.daniel@bosser.com> 
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
import sympy as sp


def main(args):
    N, a, b, c, y0 = sp.symbols("N a b c y0")
    Y0 = y0 ** 2 + b * y0 + c
    r_s = sp.cbrt(3 / (4 * sp.pi * N))
    y = sp.sqrt(r_s)
    Y = y ** 2 + b * y + c
    Q = sp.sqrt(4 * c - b ** 2)

    e1 = sp.log(y ** 2 / Y)
    e2 = 2 * b / Q * sp.atan(Q / (2 * y + b))
    e3 = sp.log((y - y0) ** 2 / (Y))
    e4 = 2 * (b + 2 * Y0) / Q * sp.atan(Q / (2 * y + b))

    eps_c = a / 2 * (e1 + e2 - b * y0 / Y0 * (e3 + e4))

    Deps_c = sp.diff(eps_c, N)
    print(Deps_c)

    return 0


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
