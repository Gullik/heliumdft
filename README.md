# HeliumDFT - Calculate helium ground state

This program, written in Python 3, calculates the ground state energy and
wave function for helium. This is done by repeatedly solving the Kohn-Sham
equation, using an effective electron potential.

### Ways of calculating exhange-correlation:
* None
* Only exchange
* Exchange-correlation (Perdew method)
* Exchange-correlation (Vosko method)

### Dependencies
* Python 3
    * Numpy
    * Scipy
    * Matplotlib
    * (Sympy, only for utils)

### Command line usage

`Usage: python3 heliumdft.py [-v] [-p]`

* -v, --verbose Show detailed progress during calculation
* -p, --plot Plot resulting wave functions using Matplotlib
